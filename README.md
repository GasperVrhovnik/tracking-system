# README #

Tracking system devloped using Python, Django, Redis Pub/Sub, PostgreSQL

### What is this repository for? ###

Celtra backend assignment

### How do I get set up? ###

* Install dependencies using pip (dependancies can be found in requirements.txt)

### Run project ###

* python manage.py runserver (tracking-service app)
* python CLIClient.py <filter_by_account_id> (example: python CLIClient.py 1)
* run redis-server.exe
* set up PostgreSQL (https://www.postgresql.org/) alternatively we can use SQLite for faset project set up

### Comments ###

* Tests made using model_bakery.
* Partly implemented replica dbs for read-only operations (need replication set up on database layer, django implmentation commented out)
* App is completely stateless wich allows for scalability

### Technological considerations ###

* In production enviorments I would probably use Google Cloud Pub/Sub service. Redis works on 'at-most-once' basis regarding delivery of messages