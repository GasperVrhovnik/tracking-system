from time import sleep
import sys

from redis import StrictRedis, ConnectionError

CHANNEL = "events"


# implement filtering - use CLI library
class PubSubClient:
    def __init__(self, filter_by_id=None):
        self.redis = StrictRedis(charset="utf-8", decode_responses=True)
        self.pub_sub = self.redis.pubsub()
        self.filter_id = filter_by_id

    def establish_connection(self):
        while True:
            print('Connecting to pub/sub...')
            try:
                self.redis.ping()
            except ConnectionError:
                sleep(1)
            else:
                self.pub_sub.subscribe(CHANNEL)
                break

        print("Connected to channel: " + CHANNEL)

    def listen(self):
        self.establish_connection()

        while True:
            try:
                message = self.pub_sub.get_message()
                if message:
                    if message.get("type") == "message":

                        data = message.get("data")
                        dict_data = eval(data)
                        if self.filter_id is not None and dict_data['account_id'] == self.filter_id:
                            # show only messages with specific account id
                            print("Message: " + data)
                        elif self.filter_id is None:
                            print("Message: " + data)
            except ConnectionError:
                print("Connection lost!")
                self.pub_sub.close()
                self.establish_connection()


def read_arguments():
    filter_arg = None
    if len(sys.argv) > 2:
        print('You have specified too many arguments')
        sys.exit()

    if len(sys.argv) == 2:
        print(sys.argv)
        if sys.argv[1].isdigit():
            filter_arg = int(sys.argv[1])
        else:
            print('Account id must me a number!')
    return filter_arg


if __name__ == "__main__":
    filter_id = read_arguments()

    client = None
    if filter_id is not None:
        client = PubSubClient(filter_by_id=filter_id)
    else:
        client = PubSubClient()
    
    client.listen()
