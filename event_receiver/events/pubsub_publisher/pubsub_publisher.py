from redis import Redis
from redis.exceptions import ConnectionError
from datetime import datetime

TOPIC = 'events'


class PubSubPublisher:

    def __init__(self):
        self.redis = Redis(charset="utf-8", decode_responses=True)

    def publishEvent(self, account, data):
        event = {
            "account_id": account.pk,
            "data": data,
            "timestamp": datetime.now().strftime('%d/%m/%Y, %H:%M:%S')
        }
        try:
            self.redis.publish(TOPIC, str(event))
            return True
        except ConnectionError:
            return False
