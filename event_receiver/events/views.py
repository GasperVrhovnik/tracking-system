from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404

from .models import Account
from .pubsub_publisher.pubsub_publisher import PubSubPublisher
from .serializers import AccountSerializer

import json


class AccountViewSet(viewsets.ModelViewSet):
    """API Resources CRUD operations for model Account"""
    """Path example: /account/"""
    """Path example: /account/1/"""
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    """API Resource for posting events"""
    """Path example: /account/1/post-event/"""
    @action(detail=True, methods=['post'], url_path='post-event', url_name='post-event')
    def post_event_for_account(self, request, pk=None):

        account = get_object_or_404(Account, pk=pk)

        # account exists
        if account.is_active:
            # account is active -> send event via pub/sub
            body_unicode = self.request.body.decode('utf-8')
            body = json.loads(body_unicode)
            data = body['data']

            publisher = PubSubPublisher()
            success = publisher.publishEvent(account, data)

            if success:
                return Response("Event sent to Pub/Sub.", status=status.HTTP_200_OK)
            else:
                return Response("Trouble connecting to the pub/sub service. Event not sent.",
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response("Account with account id: " + str(pk) + " is inactive.",
                            status=status.HTTP_400_BAD_REQUEST)
