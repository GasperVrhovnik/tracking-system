from django.db import models
from django.conf import settings


class BaseModel(models.Model):
    """Base model that provides auto-updating 'time_created' and 'time_modified' fields"""

    time_created = models.DateTimeField(auto_now_add=True, help_text='Time when record was created.')
    time_modified = models.DateTimeField(auto_now=True, help_text='Time when record was updated.')

    class Meta:
        abstract = True


class Account(BaseModel):
    """Account model with fields name and is_active"""

    name = models.CharField(max_length=50, help_text='Account name')
    is_active = models.BooleanField(null=False, blank=False, default=True, help_text='Denotes if account is active.')

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return self.name



