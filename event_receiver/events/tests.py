from django.test import TestCase
from .models import Account
from django.db.utils import IntegrityError
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
import json
from model_bakery import baker

from .serializers import AccountSerializer


class AccountTestModel(TestCase):
    """
    Class to test the model Account
    """

    def test_field_is_active_default_value(self):
        """Test if default value is set for filed is_active"""
        account = baker.make(Account)
        self.assertTrue(account.is_active)

    def test_field_name_is_mandatory(self):
        """Test if field name can not be null"""
        with self.assertRaises(IntegrityError):
            Account.objects.create(name=None)

    def test_str(self):
        """Test toString method"""
        account = baker.make(Account, name='TestAcc')
        self.assertEqual('TestAcc', account.__str__())

    def test_order(self):
        """Test ordering"""
        acc_1 = baker.make(Account, name='a')
        acc_2 = baker.make(Account, name='b')
        acc_3 = baker.make(Account, name='c')
        self.assertEqual([acc_1, acc_2, acc_3], list(Account.objects.all()))

    def test_get_active(self):
        """Test getting active accounts"""
        acc_1 = baker.make(Account, name='a', is_active=True)
        acc_2 = baker.make(Account, name='b', is_active=True)
        acc_3 = baker.make(Account, name='c', is_active=False)

        self.assertEqual(2, Account.objects.all().filter(is_active=True).count())


class AccountViewSetTest(APITestCase):
    """
    Class to test class AccountViewSet
    """

    def test_response_account_list(self):
        """Test GET Accounts list"""
        acc_1 = baker.make(Account, pk=1, name='c', is_active=False)
        url = reverse('account-list')
        response = self.client.get(url, format='json')

        accounts = Account.objects.all()
        serializer = AccountSerializer(accounts[0])

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(json.dumps(response.data))[0], serializer.data)

    def test_response_account_get_one(self):
        """Test GET Account (One)"""
        acc_1 = baker.make(Account, pk=1, name='c', is_active=False)
        url = reverse('account-detail', args=[1])
        response = self.client.get(url, format='json')

        account = Account.objects.get(pk=1)
        serializer = AccountSerializer(account)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(json.dumps(response.data)), serializer.data)

    def test_response_account_create(self):
        """Test POST Account create"""
        url = reverse('account-list')
        data = {'name': 'test-name',
                'is_active': False}
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(Account.objects.get().name, 'test-name')
        self.assertEqual(Account.objects.get().is_active, False)

    def test_response_account_delete(self):
        """Test DELETE Account"""
        acc_1 = baker.make(Account, pk=1, name='c', is_active=True)
        url = reverse('account-detail', args=[1])
        response = self.client.delete(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Account.objects.count(), 0)

    def test_response_account_update(self):
        """Test PUT Account update"""
        acc_1 = baker.make(Account, pk=1, name='c', is_active=True)
        data = {'name': 'test-name',
                'is_active': False}
        url = reverse('account-detail', args=[1])
        response = self.client.put(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Account.objects.get().name, 'test-name')
        self.assertEqual(Account.objects.get().is_active, False)

    def test_response_post_event_success(self):
        """Test POST event for Account"""
        """Needs pub/sub running"""
        acc_1 = baker.make(Account, pk=1, name='c', is_active=True)
        url = reverse('account-post-event', args=[1])
        data = {'data': 'test-data'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_response_post_event_no_connection(self):
        """Test POST event for account when there is no connection"""
        """Needs pub/sub not available"""
        acc_1 = baker.make(Account, pk=1, name='c', is_active=True)
        url = reverse('account-post-event', args=[1])
        data = {'data': 'test-data'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def test_response_post_event_if_account_does_not_exist(self):
        """Test POST event for account that does not exist"""
        acc_1 = baker.make(Account, pk=1, name='c', is_active=True)
        url = reverse('account-post-event', args=[2])
        data = {'data': 'test-data'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_response_post_event_if_account_inactive(self):
        """Test POST event for account that is inactive"""
        acc_1 = baker.make(Account, pk=1, name='c', is_active=False)
        url = reverse('account-post-event', args=[1])
        data = {'data': 'test-data'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
