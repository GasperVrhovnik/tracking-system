from rest_framework import serializers

from .models import Account


class AccountSerializer(serializers.ModelSerializer):
    """Serializer for model Account"""

    class Meta:
        model = Account
        fields = ('pk', 'name', 'is_active')
